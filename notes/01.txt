RING: A Client-Server System for Multi-User Virtual Environments

Abstract:
    server-based visibility algorithm
    entity state change update messages only sent to workstations which can potentially perceive the change.
    40x decrease in the number of messages with 1024 client entities

Introduction:
    Whenever any entity changes state or modifies the shared environment an appropiate update must
    be applied to every copy of the database in order to maintain consistant state.
    O(N*N) update messages with point-to-point connection.
    Broadcast messages make it O(N) send, but still O(N*N) received.
    Every workstations must store data and process update messages for all N entities result is
    that these systems do not scale beyond hte cpabilities of the least powerfull workstation.

Solution:
    state changes must be propagated only to hosts containing entities that can possible perceive
    the change

    RING virtual environment has entities each managed by one client workstation.
    Clients update local copies of other clients.
    Client-server design so no client-client communication.
    Vissible cell/rooms for a cell is pre calculated to be any cell visible from any possible position in the cell.
    Network bandwith requirement of the client workstations are not dependent on the total number of entities.
    Disadvantage is messages go through 1 or multiple server instead of directly to clients.

Results:
    40x decrease in messages for clients with 1024 entites and 800 rooms.
    More rooms --> less clients per room --> less messages.
    Smaller density per room is higher % decrease in #messages.

    For servers a test with 16 servers saw 74% of single server messages decreased.

