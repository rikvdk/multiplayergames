An Efficient Synchronization Mechanism for Mirrored Game Architectures

Abstract:
    Synchronization method trailing state synchronization (TTS) for distributed first person shooter games.
    Environment is a mirrored game architecture -> hybrid between centralized architecture and more scalable peer-to-peer.

Introduction:
    Client Server adds latency and server is single point of failure.
    Distributed architecture require entire game state at clients and a synchronization method.

    Synchronization methods as bucket synchronization, breathing bucket synchronization and Time Warp syncrhonization.
    Presenting new synchronization method: trailing state synchronization (TTS) for distributed FPS games.
    FPS are latency sensitive and TTS is desinged to execute commands as quickly as possible.

Background:
    Synchronization Techniques:
        In P2P clients send message between each other which in turn require synchronization method.

        Conservative Algorithms:
            Wait for all members to acknowledge that they are done with computation for the current time period.
            No guarentee that the game will advance at a regular rate.

        Optimistic Algorithms:
            Execute optimistacally and repair inconsistencies when the arrive.
            This is better suited for interactive situations.

            Time Warp synchronization takes snapshots of the state at each execution and issues rollbacks.

            Breathing algorithms same as time warp, but restricting amount of execution to an event horizon.
            Events beyond the horizon can not be consistent.
            Hard to define event horizon for FPS games.

            Mimaze is optimistic bucket synchronization.
            Events are delayed. Late events are not recovered and inconsistencies can occur.

    Mirrored Game Server:
        Client Server still widely used due to simple network code, more administrative control and IP multicast for bandwidth reduction.
        IP multicast is, however, not widely available.

        Mirrored game architecture is a hybrid architecture with multiple distributed servers.
        Clients connect to the closest mirror in a client-server fashion.
        Mirrors are connected to each other with a private network allowing a IP multicast between them.

        Multiple mirrors removes the single point of failure with client-server.
        The servers are still in control of the game publisher (not the case with P2P).

        Interesting synchronization method required as clients can close network topology wise, but far apart in game world.
        Mirrors can filter data to client to only give relavent information reducing oppertunities to cheat.

Trailing State Synchronization:
    TTS is an optimistic algorithm and must perform rollbacks when inconsistencies are detected.
    TTS runs multiple game copies at a delay. (e.g. 0ms, 100ms and 200ms)
    The later running game copies can reorder commands, find and correct inconsitencies.
    
    Copies of the same command in different states are linked so that a state Sn can find the same command from state Sn-1. 
    If a command in Sn had a different result than the same command in state Sn-1 a inconsistency is found.
    After the last state no synchronization is done, any inconsistencies after that will go undetected.

    If Sn finds an inconsistency the state Sn is copied to state Sn-1.
    All commands that were executed on Sn-1 are again appended to the pending list of Sn-1 (performed next update).
    Now Sn-1 might find inconsitencies which will synchronize state Sn-2.

    An Example of TTS:
        - look @ article with images - 

    Analysis of TTS:
        TTS is not conservative as it does not wait for all events for a time period have arrived.
        It is also different from Mimaze as it provides absolute synchronization (if commands aren't delayed longer than last State).
        TTS is more optimistic than Time Warp as it does not take snapshots for every command.
        It is possible that even though a command was late no inconsistency occurs --> looking at the result of command is key here.

        TTS performs better than other synchronization methods if:
            snapshotting is expensive.
            gap between states is small and easy to repair.
            event processing is easy.

        Choosing Synchronization Delay:
            To provide large enough synchronization window the gaps between states must be large enough if few states are used.
            Given network charateristics and game update frequency one can create a model to choose the #states and gaps.

            They now simply guess and select their values based on what they think may be good.

Performance Evaluation:
    Implementation:
        TTS implemented for Quake.
        2 mirrored servers with 3 clients connected per server.

        - mostly explanaition how they managed to implement it in Quake -

    Simulation:
        Fucked by RNG. 

        Command cost is dominated by actual execution of command in Quake simulator.
        TTS event queue management and other bookkeping only minor components of time.
        Rollback cost domnimated by copy time of contest and repairing data structures.

Conclusion:
