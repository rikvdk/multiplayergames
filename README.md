# Literature Study on Online Multiplayer Games

## Papers Used:

| Year | Title                                                                                  | Authors                               |
| ---- | -------------------------------------------------------------------------------------- | ------------------------------------- |
| 1995 | RING: A Client-Server System for Multi-User Virtual Environments                       | Thomas A. Funkhouser                  |
| 1996 | Network Topologies for Scalable Multi-User Virtual Environments                        | Thomas A. Funkhouser                  |
| 1999 | A Distributed Architecture for Multiplayer Interactive Applications on the Internet    | Diot, C. Gautier, L.                  |
| 2001 | Latency Compensating Methods in Client/Server In-game Protocol Design and Optimization | YW Bernier                            |
| 2001 | A Distributed Multiplayer Game Server System    Eric Cronin Burton Filstrup            | Anthony Kurc                          |
| 2002 | Mercury: A Scalable Publish-Subscribe System for Internet Games                        | Ashwin R. Bharambe, Sanjay Rao, ...   |
| 2002 | A Communication Architecture for Massive Multiplayer Games                             | Stefan Fiedler, Michael Wallner, ...  |
| 2002 | A generic proxy system for networked computer games                                    | Mauve, Martin and Fischer, ...        |
| 2002 | An efficient synchronization mechanism for mirrored game architectures                 | Eric Cronin, Anthony R. Kurc, ...     |
| 2002 | Aspects of Networking in Multiplayer Computer Games                                    | Jouni Smed, Timo Kaukoranta,...       |
| 2004 | Peer-to-Peer Support for Massively Multiplayer Games                                   | Knutsson, Bjorn and Lu, Honghui ...   |
| 2004 | Scalable Peer-to-Peer Networked Virtual Environment                                    | SY Hu, GM Liao                        |
| 2006 | Rokkatan: Scaling an RTS Game Design to the Massively Multiplayer Realm                | Jens Muller, Jan Hendrik Metzen, ...  |
