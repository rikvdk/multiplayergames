\documentclass[a4paper,twocolumn]{article}

\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage[table,xcdraw]{xcolor}
\usepackage[font={small}]{caption}
\usepackage[numbers]{natbib}

\setlength{\parskip}{.175cm}

\title{Literature study:\\Network solutions for real-time online multiplayer games}
\author{Rik van der Kooij, 2526314, rkj800}
\begin{document}
\maketitle

\begin{abstract}
\textbf{The multiplayer feature has become increasingly important for the survival of video games.
Different characteristics of real-time multiplayer systems for games makes it impossible to create a perfect networking solution. 
Games need to have low latency in order to get players immersed in the game world.
Scalability of resources is required for massive multiplayer games with thousands of concurrent players.
Consistency between game states is required for all players to have the same view of the game.
Multiple copies of networking resources can increase the robustness of the system. 
We look at latency compensation to increase responsiveness at the cost of inconsistencies.
Bucket synchronization and trailing state synchronization are shown to create consistent states between multiples copies in the system.
Lastly, we look at interest areas of players in order to filter irrelevant data increasing scalability of the system.}
\end{abstract}

\section{Introduction}
\label{sec:introduction} 
\begin{table*}
\centering
\caption{Main problem each paper is trying to solve with their networking solution. Smed et al. [10] describe all the aspects of networking in a multiplayer game and do not propose a solution to a problem.}\label{table:goals}
\begin{tabular}{ l|c c c c c c c c c c c c c}
& \cite{01_ring} & \cite{02_network_topologies} & \cite{03_distributed_architecture} & \cite{04_latency} & \cite{05_distributed_server} & \cite{06_mecury} & \cite{07_communication} & \cite{08_proxy} & \cite{09_synchronization} & \cite{10_aspects} & \cite{12_scalable_p2p} & \cite{13_scaling_rts}\\\hline\hline
Scalability & $\times$ & $\times$ &          &          &          & $\times$ & $\times$ &          &          &          & $\times$ &          \\
Consistency &          &          & $\times$ &          & $\times$ &          &          &          & $\times$ &          &          & $\times$ \\
Responsiveness &       &          &          & $\times$ &          &          &          &          &          &          &          &          \\
Robustness  &          &          &          &          &          &          &          & $\times$ &          &          &          &          \\
\end{tabular}
\end{table*}

With the development of the internet over the last two decades the popularity of online multiplayer video games have increased significantly \cite{10_aspects}.
Multiplayer video games are computer generated worlds that allow simultaneous interactions of multiple participants.
They started as large scale combat simulations for the US Government.
Multiplayer has become increasingly important for the survival of video games.

%Networking requirements of a game depends on the type of the video game.
Different characteristics for real time multiplayer games are discussed in the literature.

%todo rewrite
%\textbf{Responsiveness:} Games are simulations of fictional worlds. Responsiveness of game entities is important for the immersion of players.
\textbf{Responsiveness:} Responsiveness is the amount of delay a player experiences before his input is processed by the game. High responsiveness is important for the immersion of players.

\textbf{Scalability:} Scalability in online multiplayer games is concerned with the number of players simultaneously in a game.

\textbf{Consistency:} Consistency between players view of the game world is required to have meaningful player interaction. Synchronization mechanisms are required to handle the latencies of players.

\textbf{Robustness:} A game is considered robust if it is playable as long as the players' computers and their connection is operational. No central point of failure must be present in the system.

The requirements of these characteristics depends on the type of video game.
Top-down viewed games, mostly strategy or role playing games, can tolerate higher latencies, but require more entities in a single game instance.
First-person viewed games needs to be responsive, but fewer players are present in a single game.
Game developers will need to choose suitable networking solutions for the requirements of their game.

Table \ref{table:goals} shows the main goal of each discussed paper in this study.
Most papers having scalability as their main goal shows that scalability is highly researched for online multiplayer games.
This is to be expected as many commercial multiplayer games with tens or hundreds of players have been operating for a long period of time.
While massive multiplayer games with thousands or even tens of thousands of concurrent players are scarce.
It shows that scaling a multiplayer game to be a massive multiplayer game is a challenge.

The rest of this study is organized as follows:
Section \ref{sec:architecture} explains the different server architectures used in the discussed papers for the multiplayer games.
Section \ref{sec:responsiveness} discusses responsiveness and how it affects consistency between players.
Section \ref{sec:synchronization} explains how consistency for players can be established.
Section \ref{sec:scalability} shows that players do not need full information of large game worlds. With message filtering based on player interest scalability of the system can be increased.
Section \ref{sec:conclusion} formulates some conclusion.

\section{Server architectures}
\label{sec:architecture}

\begin{table*}
\centering
\caption{Server architectures discussed for each paper. Publish-subscribe is a messaging pattern used on top of the distributed architecture.}\label{table:architecture}
\begin{tabular}{ l|c c c c c c c c c c c c c}
& \cite{01_ring} & \cite{02_network_topologies} & \cite{03_distributed_architecture} & \cite{04_latency} & \cite{05_distributed_server} & \cite{06_mecury} & \cite{07_communication} & \cite{08_proxy} & \cite{09_synchronization} & \cite{10_aspects}  & \cite{12_scalable_p2p} & \cite{13_scaling_rts}\\\hline\hline
Client-server              & $\times$ &          &          & $\times$ &          &          &          &          &          & $\times$ &          &          \\
Proxy                      &          & $\times$ &          &          & $\times$ &          &          & $\times$ & $\times$ & $\times$ &          & $\times$ \\
Distributed                & $\times$ &          & $\times$ & $\times$ & $\times$ & $\times$ & $\times$ &          &          & $\times$ & $\times$ &          \\
\textit{Publish-subscribe} &          &          &          &          &          & $\times$ & $\times$ &          &          &          &          &          \\
\end{tabular}
\end{table*}

Three different server architectures are present in the chosen papers: client-server, proxy and distributed.
Table \ref{table:architecture} shows which architectures are discussed for each paper in the literature.
The table shows also the publish-subscribe messaging pattern that is used on top of the distributed server architecture for two papers.

\subsection{Client-server}
\label{sub:client-server}

Client-server model uses a single server where all clients connect to.
All communication is going through the server, making it the most important element for the clients.
This model is simple to implement as consistency of game state is relatively easy with a single authoritative server \cite{08_proxy}.
It also gives game publishers full control of the servers making it possible to easily charge players based on play time.
For these reasons client-server is widely used for commercial games.

The Problem with client-server model is the server being a single point of failure.
Limited compute power and bandwidth on the server makes it less scalable.
As messages are routed through the server it adds latency over the cost of sending directly to other clients.
Many papers discussed in this study try to overcome the problems of the client-server model.

\subsection{Proxy}
\label{sub:proxy}

Proxy or mirrored game servers use multiple connected servers.
Clients connect to one of the proxy servers and update messages are forwarded through the proxies to other clients.
The proxy architecture helps with congestion control, robustness and providing fairness \cite{08_proxy}.
Proxy servers are interconnected making it possible to route traffic around congested areas.
If all routes are full the proxies can handle access control while other proxies continues operating without being flooded with connection requests.
Robustness is achieved as the single point of failure from client-server is eliminated.
If proxies can detect outages fast, transparent rerouting of clients to a different proxy can hide faulty network behavior.
For a game to be competitive all users must have the same chance of winning and cheating must be preventable.
As proxy servers are all still under the control of the game publishers this can be ensured.
The resource bottleneck of a single server is now split over multiple proxies servers increasing scalability.

Each proxy server is required to keep a copy of the game state.
A synchronization method is required to keep these game states consistent.
Latency is increased as messages are now routed through multiple proxies.
The proxy architecture keeps the positive aspects of client-server while being able to handle more players.

\subsection{Distributed}
\label{sub:distributed}

Distributed or peer-to-peer architectures has all clients connected directly to each other.
In the simplest form each update is sent to all other clients.
Failure of any nodes in has no effect on other participants making this architecture robust.
This architecture has the advantage of lower latencies as messages do not go through a server.
It also eliminates the concentrated bandwidth consumption of servers.

Clients joining the game will have to be able to find the distributed network.
Without a single authority over the game state synchronization methods are required for consistent views of the game world.
Cheating is also problematic as other client machines cannot be trusted.
In the end distributed architecture improves the real-time properties of the application at the cost of consistency \cite{03_distributed_architecture}.

\section{Responsiveness}
\label{sec:responsiveness}

In order for players to feel immersed in the game world responsiveness has to be high.
Role playing games can hide latency with casting times for spells.
First person shooters cannot use this method as actions happen instantly.
Also, the player is in direct control of their character making delays very noticeable.

A simple client-server multiplayer implementation would have clients send user commands to the server.
The server executes these commands and sends updated position of all movable objects back to the client.
When the client receives these updates it can render the scene with updated object position.
A problem with this simple implementation is that the client has to wait for the entire round trip time before change is shown on their screen.

\subsection{Client-side prediction}
\label{sub:client-sideprediction}

Bernier, Yahn W \cite{04_latency} shows how they used client side prediction in Half-Life to make movement of the player more responsive.
Using the last known acknowledged position from the server we have an exact starting position of our player.
This position will be somewhere in the past due to the round trip time from the server.
The client has, therefore, already issued new movement commands to the server.
These commands are played out on the client as if the server has acknowledged them.
Result is instant response for player movement with only small corrections on new incoming acknowledgements from the server as clients run the same movement code as the server.

Firing weapons can be predicted in the same way as movement.
Simply assuming the server acknowledges weapon firing, we can immediately simulate sound and visual effects.
Problem is having to aim in front of your targets as the actual result of your shot is still subject to latency.
Getting a feel for your latency is difficult especially with fluctuating latencies.

\subsection{Latency compensation}
\label{sub:latencycompensation}

In order to resolve the targeting issue Bernier, Yahn W \cite{04_latency} uses interpolation and lag compensation.
Moving objects are shown somewhat in the past with respect to the last received position of that object.
Interpolation is done on the last known position of the object and the one before that when rendering a frame.
As the object approaches to the last acknowledged position, we get a new update from the server.

Interpolation of targets adds another type of latency for the players.
Lag compensation is a method of normalizing the server-side state of the world to the world state as the player saw it when issuing the command.
Before user commands are executed, the server computes the latency of the player.
Server searches the history to get the last world update that was sent and received by the player.
From that update move each player backwards in time to exactly where they were when the player command was issued while keeping latency and interpolation in account.
The server then executes the clients commands including weapon firing.
Players can now aim directly at the targets without having to take latency into account.

While this gives full responsiveness to the player it creates inconsistencies in the system.
As a player being shot on is already further ahead it may already have been moved from that spot.
This can feel like being shot from behind a corner.
Authors chose this inconsistency with high responsiveness over problems before where we had to shoot in front of the targets.

\subsection{Dead reckoning}
\label{sub:deadreckoning}

Another way to improve responsiveness is to hide latency using dead reckoning.
Dead reckoning is a method to estimate one's position using a known starting point and velocity.
Sending the velocity along with position makes it possible to predict future positions of objects.
Dead reckoning can be used when movement is not interpolated as with lag compensation or when update messages arrive too late.

Correctness of dead reckoning depends on the jerkiness of the objects.
With correct prediction messages can be sent only when dead reckoning exceeds some error threshold.
None of the discussed papers have invested in implementing a sophisticated dead reckoning algorithm.
Diot et al. \cite{03_distributed_architecture}; Smed et al. \cite{10_aspects}; M{\"u}ller and Gorlatch. \cite{13_scaling_rts}
all replay the last known command from the server.

\section{Synchronization}
\label{sec:synchronization}

Proxy and distributed server architectures remove the bottleneck of a single server, but require a special synchronization method in order to provide consistency between all copies of the game state.
Conservative synchronization algorithms wait for all clients to acknowledge that they are done with a current time period.
Waiting for all clients means that there is no guarantee that the game will advance at a regular rate.
This makes conservative algorithms not suited for real-time multiplayer games.
Optimistic algorithms execute commands optimistically.
They try to find inconsistencies and repair any differences.

\subsection{Bucket synchronization}
\label{bucketsynchronization}

Bucket synchronization by Diot and Gautier \cite{03_distributed_architecture} is a distributed synchronization method which guarantees consistency regardless of latency.
Time is sampled in periods of 40 ms.
Each time period is represented with a bucket containing update messages.
Locally created updates are stored in the first bucket 100 ms in the future.
Remote update messages are also placed 100 ms in the future from which the message has been issued.
At the end of every bucket interval all update messages in that bucket are executed.
Remote messages arriving within 100 ms will, therefore, be executed at the same time as local updates.
Messages arriving after 100 ms will be too late as the corresponding bucket has already been played out.
They are still put in the bucket because it is used for dead reckoning as described in Subsection \ref{sub:deadreckoning}.

Evaluation uses drift distance to represent distance between local position of an entity and the same entity displayed remotely.
In their simulation 15\% of the update messages arrived too late.
Resulting in drift distance higher than zero.
However, a single update message on time will reset the drift distance to zero.

\subsection{Trailing state synchronization}
\label{trailingstatesynchronization}

Cronin et al. \cite{05_distributed_server, 09_synchronization} describe bucket synchronization to be loosely consistent and not well suited to the demands of a real-time multiplayer games.
They introduce Trailing State Synchronization (TSS) designed for real-time multiplayer games.
TSS achieves better responsiveness and scalability with near-perfect consistency.
TSS is like bucket synchronization an optimistic synchronization algorithm.

The game is simulated multiple times with delays 0 ms, 100 ms and 200ms.
Game simulation with 0 ms delay is shown to the users.
Later running game simulations can reorder out of order received update messages.
Therefore, later running game simulations can find and correct inconsistencies in the game simulation with a shorter delay.

As commands have to be run for all three game simulations they are stored.
The game commands are linked between the different simulation so that a simulation $S_{n}$ can find the same command from simulation $S_{n-1}$.
If a command in the simulation with 200 ms delay ($S_{n}$) had a different result than the same command in the simulation with 100 ms ($S_{n-1}$) an inconsistency is found.
The game state of $S_{n}$ is copied to $S_{n-1}$.
All commands that were executed on $S_{n-1}$ are appended to the pending list of $S_{n-1}$ so that the inconsistency is repaired on the next update.
This process of inconsistency finding is recursive as $S_{n-1}$ can now find and repair inconsistencies for $S_{n-2}$.
After the last running simulation, no synchronization is done and any inconsistency after 200 ms will go undetected.

Advantage of TSS is that out of order commands with the same results will not trigger inconsistency repairing.
For example, movement commands for different players can be executed in different orders while still having the same result.

Evaluation of their implementation for the first person shooter Quake showed a problem with random events. 
Quake uses a random number generator for random events as item placements.
Out of order random events create different random numbers resulting in inconsistencies.
Even with this added inconsistencies their simulation showed that TSS works for low latency multiplayer games.

\subsection{Eventual consistency}
\label{eventualconsistency}

M{\"u}ller and Gorlatch \cite{13_scaling_rts} adopted eventual consistency in a proxy architecture.
Only a single proxy is allowed to alter specific parts of the game state which prevents concurrent write access.
Proxies have authority over the directly connected clients.
They can perform and acknowledge movement commands to their directly connected clients.
The proxy communicates this game state change to the other proxies.

Interaction between players cannot be handled by a single proxy as two different proxies can have authority over the two interacting players.
Damage interaction will first be checked for validation by the proxy of the player who generated the command.
It sends an acknowledgement message of the command back to the player and forwards the damage to the other proxies.
The other proxies will update and check for interaction with any of its players.
If there is interaction with any of its players it will update those players and send updates to the other proxies.

Collision between players is problematic as position data from clients on other proxies might be outdated due to latency.
The result is players colliding without corresponding proxies knowing.
In order to catch collisions before it happens they use virtual circle areas around the players to increase collision range.
The size of this circular area is adjusted to the latency so that the game is able to run flawlessly.

\section{Scalability}
\label{sec:scalability}

Scalability in video games is concerned with the number of simultaneous players in the game.
Massive multiplayer games can have thousands of concurrent players making scalability for these games the main priority.
Compute power and networking bandwidth are limited available resources in the system.
If resources are concentrated in the system it creates bottlenecks making the system not scale to large numbers of players.

Simple multiplayer game systems send update messages to all players.
With large game worlds players may only be interested in only a small portion of the game world.
All papers discussed in this section will be using some solution to manage interests for players.
Only sending relevant game world data, reduces bandwidth requirement by using more compute power to calculate interests for players.

\subsection{Splitting game world}
\label{sub:splitting}

Players in video games have limited visibility of the game world.
Splitting the map makes it possible to only send updates of the visible world regions to players.
Fukhouser \cite{01_ring} was one of the first to describe how the game world can be split up to decrease the number of update messages.
He used simple rectangular rooms connected with rectangular corridors.
By precalculating visibility in the world, we know which rooms and corridors are visible from each other.
Using a single server makes filtering easy as we only have to know in which room each player is located.

In his simulation with 1024 players and 800 rooms he reduced the amount of messages 40 times for clients.
The amount of messages now scaled with the density of the rooms instead of number of clients.

In a second paper Fukhouser \cite{02_network_topologies} describes how distributed and proxy architecture are improved using the visibility filtering.
Distributed architecture requires peers to know which other peers are interested in their position updates.
Peers must, therefore, maintain an up-to-date mapping of entities to rooms.
As each peer must map all peers to rooms, updating this mapping or in words switching rooms must be infrequent for this design to work.
If the network, however, supports multicast we can instead join and leave multicast groups as players enter new rooms.
No multicast service is, however, generally available for end-users on the internet.

Finally, he describes how proxy servers would be improved using the visibility filtering.
Parts of the map were mapped to each proxy server with clients connectionless connected using unicast messages.
This benefits the client as it now only has to send a single message to a proxy.
Simulation for the proxy architecture showed that the server to server communication was 20 times decreased.

\subsection{Publish-subscribe}
\label{sub:publish-subscribe}

Splitting the map creates the possibility to filter messages on position.
Some information data as total kills and deaths for each player might not be limited to world regions.
In order to get more control over message filtering Bharambe et al. \cite{06_mecury} use the publish-subscribe pattern on top of a distributed architecture.
In a publish-subscribe system publishers inject publications into the network, while subscribers register their interests by sending subscriptions.
Routers in the network deliver a publication only to the subscribers whose subscriptions match the publication.

Responsibility of the publish-subscribe pattern is divided among all nodes by partitioning them in groups which they call attribute hubs.
Each attribute hub is responsible for an attribute in the overall schema.
The nodes inside such a hub are logically arranged in a circle.
Every node keeps track of its successor and predecessor.
Each node is responsible for a range of attribute values, for example an attribute $a$ with values from 100 to 200.

Using position for a 2d game world we have two attributes: $x$ and $y$.
Two attribute hubs are used: one hub for the $x$ and one for $y$.
A subscription gets routed to any one of the attribute hubs $Hx$ or $Hy$.
Publications are, however, routed to both the hubs as any one of them could have stored relevant subscriptions.

A problem with the implementation lies with routing.
Nodes in a hub only store successor and predecessor resulting in $\mathcal{O}(n)$ routing times.
Average routing takes n/2 hobs.
It shows in their results with a 400 ms delay with 50 nodes and 900 delay with 100 nodes.
Due to this delay their solution is not suitable for real time games.

Systems like Chord would be an improvement as they have faster routing.
However, in chord identifies the node in charge of an attribute by using a hash over the attribute value.
In their case they want to be able to use ranges of values making Chord not flexible enough for their requirements.

Another publish-subscribe implementation on top of a distributed architecture is presented by Fiedler et al. \cite{07_communication}.
Instead of splitting the world into $x$ and $y$ ranges, they split the map into hexagons.
Hexagons can be mapped to a channel ID for the publish-subscribe pattern.

Game communication is split into two types of messages: environmental and interaction.
Environmental updates are used for static fixed items on the map.
It has low amount of updates and small delays do not matter.
Interaction updates is for player movement and player interaction.
These messages happen more frequent and need to be delivered fast.

Players in their game can interact close to their own position while being able to see further in the distance where only environmental data is required.
For each hexagons two publish-subscribe channels are created: one for environment and one for interaction updates.
It is then possible to subscribe to interaction channel of the players' current hexagon and subscribing to all neighboring environmental channels.
This splitting of messages reduces the load on the system because we only subscribe to a single interaction subscription and 7 (current hexagon and 6 neighbors) environmental channel subscriptions.

While moving closer to the border and corners of the hexagons players will be required to shortly subscribe to multiple interaction channels.
The use of hexagons gives us 2 extra interaction channels that have to be subscribed to when approaching a corner.
In their test the compare their hexagon map splitting to squares.
A square has 8 neighbors and in corners 3 extra interaction channels need to be subscribed.
Their simulation showed that the hexagon splitting of the map resulted less subscriptions required to environmental channels.
They also found that smaller tile sizes results in a smaller percentage of the map needed to be subscript to.


% two images next to each other
%\begin{figure}[h]\centering
%   \begin {minipage}{0.24\textwidth}
%     \frame{\includegraphics[width=\linewidth]{highwayExample.jpg}}
%     \caption{Frame from highway sequence}\label{fig:exampleHighway}
%   \end{minipage}
%   \begin{minipage}{0.24\textwidth}
%     \frame{\includegraphics[width=\linewidth]{trafficExample.jpg}}
%     \caption{Frame from traffic sequence}\label{fig:exampleTraffic}
%   \end{minipage}
%\end{figure}

% Single image
%\begin{figure}[h]
%  \includegraphics[width=\linewidth]{tarjectoryPixels.png}
%  \caption{Trajectory of pixel blocks for traffic sequence}
%  \label{fig:pixelTrajectory}
%\end{figure}

\section{Conclusions}
\label{sec:conclusion}
We have discussed multiple solutions for real-time online multiplayer games.
The client-server, proxy and distributed server architectures were briefly discussed.
All three architectures have advantages and disadvantages and no single best architecture exists.

We have seen how responsiveness was chosen over consistency for the first person shooter Half-Life.
Client-side prediction and intelligent server design with lag compensation gave players full responsiveness in a first person viewed game.
Dead reckoning can be used to increase responsiveness when update message arrive late by predicting movement of objects.

Three different consistency algorithms have been discussed.
Bucket synchronization showed how simple time sampling in buckets can create consistency in a distributed system.
Trailing state synchronization created near-perfect consistency.
Eventual consistency was discussed for a proxy server architecture.
Assigning single proxies to have authority over updating game objects prevented concurrent write access.

In order to create massive multiplayer game systems with thousands of concurrent players scalability was described.
Message filtering based on the visibility or interest of players reduced bandwidth requirements of the system.
Using a publish-subscribe message pattern showed how interest management of any game world attribute can be realized.

\bibliographystyle{unsrt}
\bibliography{litstudy}

\end{document}
